import sys, re

template = """
#ifdef VERSION_H
#define VERSION_H

#define VERSION_TAG         "{version_tag}"
#define MAJOR_VERSION       {major_version}
#define MINOR_VERSION       {minor_version}
#define PATCH_VERSION       {patch_version}

#endif
"""
def main(argv):
    sha = argv[0]
    
    old_tag = ""
    major = 0
    minor = 0
    patch = 0

    try:
        old_tag = argv[1]
    except IndexError:
        raise Exception("No tag available")
    
    try:
        labels = argv[2:]
    except IndexError:
        raise Exception("No labels available")

    
    result = re.search(r"v(\d+).?(\d*).?(\d*)", old_tag)
    if result:
        major = int(result.group(1))
        minor = int(result.group(2))
        patch = int(result.group(3))
    else:
        raise Exception("Wrong tag format")

    
    if "Major" in labels:
        major += 1
        minor = 0
        patch = 0

    elif "Minor" in labels:
        minor += 1
        patch = 0

    elif "Patch" in labels:
        patch += 1
        
    tag = f"v{major}.{minor}.{patch}"
    print(tag, end ="")
    
    commit_h = template.format(
        version_tag=tag,
        major_version=major,
        minor_version=minor,
        patch_version=patch
    )
    
    with open("commit.h", "w") as file:
        file.write(commit_h)
        
    with open("tag.new", "w") as file:
        file.write(tag)

if __name__ == "__main__":
    if len(sys.argv) > 1:
        main(sys.argv[1:])
    else:
        raise Exception("Argument list empty!")