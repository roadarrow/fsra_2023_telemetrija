#include "fsra_eth.h"

bool eth_connected;
AsyncUDP eth_udp;

void eth_setup() {
    WiFi.onEvent(eth_on_event);
    ETH.begin();
    ETH.config(
            eth_local_ip_address,
            eth_gateway_ip_address,
            eth_subnet_mask,
            eth_dns_ip_address
    );
    printf("Ethernet: Setup complete\n");
}

void eth_on_event(WiFiEvent_t event) {
    switch (event) {
        case ARDUINO_EVENT_ETH_START:
            printf("Ethernet: Started\n");
            //set eth hostname here
            ETH.setHostname("FSRA Telemetry");
            break;
        case ARDUINO_EVENT_ETH_CONNECTED:
            printf("Ethernet: Connected\n");
            break;
        case ARDUINO_EVENT_ETH_GOT_IP:
            printf("Ethernet: MAC: ");
            printf("%s", ETH.macAddress().c_str());
            printf(", IPv4: ");
            printf("%s", ETH.localIP().toString().c_str());
            if (ETH.fullDuplex()) {
                printf(", FULL_DUPLEX");
            }
            printf(", ");
            printf("%d", ETH.linkSpeed());
            printf(" Mbps\n");
            eth_connected = true;
            break;
        case ARDUINO_EVENT_ETH_DISCONNECTED:
            printf("Ethernet: Disconnected\n");
            eth_connected = false;
            break;
        case ARDUINO_EVENT_ETH_STOP:
            printf("Ethernet: Stopped\n");
            eth_connected = false;
            break;
        default:
            break;
    }
}
