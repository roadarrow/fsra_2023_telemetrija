#ifndef FSRA_ETH_H
#define FSRA_ETH_H

#include <Arduino.h>
#include "WiFi.h"
#include "ETH.h"
#include <AsyncUDP.h>

#include "FSRA_WIFI/fsra_wifi.h"
#include "config.h"


void eth_setup();

void eth_on_event(WiFiEvent_t event);

#endif //FSRA_ETH_H
