#include "fsra_sd.h"

String sd_current_log_filename;
File sd_current_log_file;
//uint sd_write_count = 0;
bool sd_initialized = false;

extern volatile SemaphoreHandle_t sd_flush_timer_semaphore;
extern bool ftp_connected;

void sd_setup(){
    printf("SD: Starting setup...\n");

    if(!SD_MMC.begin("/sdcard", false, false, 52000, 5))
    {
        printf("SD: Error! Failed to initialize\n");
    }
    else {
        sd_initialized = true;
        printf("SD: Setup complete\n");
        printf("SD: card type= ");
        switch (SD_MMC.cardType()) {
            case CARD_NONE:
                printf("Slot empty\n");
                sd_initialized = false;
                break;
            case CARD_MMC:
                printf("MMC\n");
                break;
            case CARD_SD:
                printf("SD\n");
                break;
            case CARD_SDHC:
                printf("SDHC\n");
                break;
            case CARD_UNKNOWN:
                printf("Unknown\n");
                break;
            default:
                printf("Unknown\n");
        }

        printf("SD: Card size: ");
        printf("%d MB\n", (uint32_t) (SD_MMC.cardSize() / 1000000));
        printf("SD: Total size: ");
        printf("%d MB\n", (uint32_t) (SD_MMC.totalBytes() / 1000000));
        printf("SD: Used: ");
        printf("%d MB\n", (uint32_t) (SD_MMC.usedBytes() / 1000000));

    }
}

void sd_log(comm_message_t& comm_message){
    if(!sd_initialized || ftp_connected){
        return;
    }

    sd_open_file();

    for(int i=0; i < comm_message.length; i++){
        sd_current_log_file.printf("%02X", comm_message.data[i]);
    }
    sd_current_log_file.print("\n");

//    sd_write_count++;
//    if(sd_write_count > sd_flush_frequency){
//        sd_current_log_file.flush();
//        sd_write_count = 0;
//    }
    if(xSemaphoreTake(sd_flush_timer_semaphore, 0) == pdTRUE){
        sd_current_log_file.flush();
    }
}

void sd_open_file(){
    if(!sd_current_log_file){
        char sd_filename_buffer[40];
        sprintf(sd_filename_buffer, "/%X.log", esp_random());
        sd_current_log_filename = String(sd_filename_buffer);
        sd_current_log_file = SD_MMC.open(sd_current_log_filename, FILE_APPEND);
        printf("SD: %s file created\n", sd_filename_buffer);
    }
}

void sd_close_file(){
    if(sd_current_log_file){
        sd_current_log_file.flush();
        sd_current_log_file.close();
    }
}