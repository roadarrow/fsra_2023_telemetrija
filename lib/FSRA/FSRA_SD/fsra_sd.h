#ifndef FSRA_SD_H
#define FSRA_SD_H

#include <Arduino.h>

#include "FS.h"
#include "SD_MMC.h"

#include "FSRA_COMM/fsra_comm.h"
#include "config.h"
#include "utils.h"


void sd_setup();
void sd_log(comm_message_t& comm_message);

void sd_open_file();
void sd_close_file();


#endif //FSRA_SD_H
