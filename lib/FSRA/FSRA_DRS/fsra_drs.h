#ifndef FSRA_DRS_H
#define FSRA_DRS_H

#include <Arduino.h>
#include "FSRA_CAN/fsra_can.h"

#include "iot_servo.h"


#define DRS_CAN_MESSAGE_ID 0xB00

typedef enum {
    DRS_OPENED = 0,
    DRS_CLOSED = 1
}DRS_State;


void drs_setup();

void drs_task_handler(void* parameter);
void drs_callback(CAN_frame_t can_message);
void drs_set_state(DRS_State state);


#endif //FSRA_DRS_H
