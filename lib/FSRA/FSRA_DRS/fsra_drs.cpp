#include "fsra_drs.h"

extern QueueHandle_t drs_command_queue;
extern TaskHandle_t drs_task;

void drs_setup() {
    servo_config_t servo_config = {
            .max_angle = DRS_SERVO_MAX_ANGLE,
            .min_width_us = DRS_SERVO_MIN_WIDTH_US,
            .max_width_us = DRS_SERVO_MAX_WIDTH_US,
            .freq = DRS_SERVO_FREQUENCY,
            .timer_number = LEDC_TIMER_0,
            .channels = {
                    .servo_pin = {
                            DRS_SERVO_GPIO
                    },
                    .ch = {
                            LEDC_CHANNEL_0,
                    },
            },
            .channel_number = 1,
    } ;

    iot_servo_init(LEDC_LOW_SPEED_MODE, &servo_config);
    // setting DRS servo to midpoint
    iot_servo_write_angle(LEDC_LOW_SPEED_MODE, 0, DRS_SERVO_MAX_ANGLE/2.0);
    printf("DRS: Neutral\n");

    xTaskCreatePinnedToCore(
            drs_task_handler,   /* Function to implement the task */
            "DRS Task",   /* Name of the task */
            2000,          /* Stack size in words */
            NULL,             /* Task input parameter */
            1,                   /* Priority of the task */
            &drs_task,        /* Task handle. */
            DRS_TASK_RUNNING_CORE);
}

void drs_task_handler(void* parameter) {
    delay(2000); //! Wait so that the servo will stay at midpoint

    DRS_State state = DRS_CLOSED;
    while(true) {

#ifndef DRS_TEST
      // get from drs command mailbox
      // TODO: DRS timeout define
      if (xQueueReceive(drs_command_queue, &state, DRS_CONTROL_TIMEOUT * portTICK_PERIOD_MS) == pdPASS) {
          drs_set_state(state);
      }
      else {
          // Timeout on DRS message, enter a safe state
          printf("DRS: Message timeout\n");
      }
#else
      drs_set_state(static_cast<DRS_State>(state));
      state = static_cast<DRS_State>(state ^ 1);
      delay(1000);
#endif
    }
}

void drs_callback(CAN_frame_t can_message) {
    if (can_message.MsgID != DRS_CAN_MESSAGE_ID || can_message.bus != CAN1) {
        return;
    }

    DRS_State state = (DRS_State)can_message.data.u8[0];
    if (state > 1 & can_message.FIR.B.DLC != 1) {
        return;
    }

    xQueueSend(drs_command_queue, &state, 0 * portTICK_PERIOD_MS);
}

void drs_set_state(DRS_State state) {
    if (state > 1){
        return;
    }

    if (state == DRS_OPENED) {
#ifdef DRS_DEBUG
        printf("DRS: Opened\n");
#endif
        iot_servo_write_angle(LEDC_LOW_SPEED_MODE, 0, DRS_SERVO_MAX_ANGLE/2.0 + DRS_SERVO_OPENED_OFFSET);
    }
    else if (state == DRS_CLOSED) {
#ifdef DRS_DEBUG
        printf("DSR: Closed\n");
#endif
        iot_servo_write_angle(LEDC_LOW_SPEED_MODE, 0, DRS_SERVO_MAX_ANGLE/2.0 + DRS_SERVO_CLOSED_OFFSET);
    }
}
