#ifndef FSRA_WIFI_STRUCTS_H
#define FSRA_WIFI_STRUCTS_H

#include <Arduino.h>

typedef struct
{
    unsigned frame_ctrl : 16;  // 2 bytes / 16 bit fields
    unsigned duration_id : 16; // 2 bytes / 16 bit fields
    uint8_t addr1[6];          // receiver address
    uint8_t addr2[6];          //sender address
    uint8_t addr3[6];          // filtering address
    unsigned sequence_ctrl : 16; // 2 bytes / 16 bit fields
} wifi_ieee80211_mac_hdr_t;    // 24 bytes

typedef struct {
    uint16_t element_id;
    uint16_t length;
    uint8_t data[512];
} wifi_ieee80211_fsra_vaf_t;

typedef struct
{
    wifi_ieee80211_mac_hdr_t hdr;
    unsigned category_code : 8; // 1 byte / 8 bit fields
    uint8_t oui[3]; // 3 bytes / 24 bit fields
    wifi_ieee80211_fsra_vaf_t fsra_frame;
    uint8_t payload[0];
} wifi_ieee80211_packet_t;

typedef enum {
    ASSOCIATION_REQ,
    ASSOCIATION_RES,
    REASSOCIATION_REQ,
    REASSOCIATION_RES,
    PROBE_REQ,
    PROBE_RES,
    NU1,  /* ......................*/
    NU2,  /* 0110, 0111 not used */
    BEACON,
    ATIM,
    DISASSOCIATION,
    AUTHENTICATION,
    DEAUTHENTICATION,
    ACTION,
    ACTION_NACK,
} wifi_mgmt_subtypes_t;

typedef struct {
    unsigned protocol: 2;
    unsigned type: 2;
    unsigned subtype: 4;
    unsigned to_ds: 1;
    unsigned from_ds: 1;
    unsigned more_frag: 1;
    unsigned retry: 1;
    unsigned pwr_mgmt: 1;
    unsigned more_data: 1;
    unsigned wep: 1;
    unsigned strict: 1;
} wifi_ieee80211_frame_control_t;



#endif //FSRA_WIFI_STRUCTS_H
