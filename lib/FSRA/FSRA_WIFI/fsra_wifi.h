#ifndef FSRA_WIFI_H
#define FSRA_WIFI_H

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncUDP.h>
#include <esp_wifi.h>
#include <esp_now.h>

#include "config.h"
#include "FSRA_CAN/fsra_can.h"

#include "fsra_wifi_structs.h"

static const uint8_t wifi_ieee80211_raw_header[] = {
        0xD0, 0x00,                                               // 0-1: Action Frame
        0x00, 0x00,                                               // 2-3: Duration
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,      // 4-9: Destination address
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  // 10-15: Source address,
        0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,  // 16-21: BSSID
        0x00, 0x00,                                             // 22-23: Sequence/fragment number
        0x7F,                                                        // 24: Category code (127)
        0x46, 0x53, 0x52,                                  // 25-27: OUI (FSRA)
        0xDD,                                                        // 28: Element ID (221)
        0x00,                                                        // 29: Padding
        0x00, 0x00,                                             // 30-31: Length

};

void wifi_setup();


void wifi_ieee802111_fsra_tx(uint8_t * data, uint length);



#endif //FSRA_WIFI_H
