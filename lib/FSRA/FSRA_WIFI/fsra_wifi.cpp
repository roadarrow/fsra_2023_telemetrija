#include "fsra_wifi.h"

extern AsyncUDP eth_udp;
extern bool eth_connected;

void wifi_setup() {
    printf("Wi-Fi: Starting setup...\n");

    wifi_init_config_t wifi_config = WIFI_INIT_CONFIG_DEFAULT();
    wifi_config.ampdu_tx_enable = 0;
    // increased Wi-Fi TX buffers
    wifi_config.dynamic_tx_buf_num = 32;
    wifi_config.static_tx_buf_num = 32;

    // initialize WiFi config
    ESP_ERROR_CHECK(esp_wifi_init(&wifi_config));

    // set raw IEEE802.11 rate
    ESP_ERROR_CHECK(esp_wifi_config_80211_tx_rate(WIFI_IF_STA, WIFI_PHY_DATA_RATE));

    // start WiFi
    ESP_ERROR_CHECK(esp_wifi_start());

    // set ESP-NOW TX rate and output power
    ESP_ERROR_CHECK(esp_wifi_config_espnow_rate(WIFI_IF_STA, WIFI_PHY_DATA_RATE));
    WiFi.setTxPower(WIFI_PHY_TX_POWER);

    // initialize ESP-NOW
    WiFi.mode(WIFI_STA);
    ESP_ERROR_CHECK(esp_now_init());

    esp_now_peer_info_t wifi_espnow_peer_info = {
            .peer_addr = WIFI_BROADCAST_ADDRESS, // set to broadcast address
            .channel = WIFI_CHANNEL,
            .ifidx = WIFI_IF_STA,
            .encrypt = false,
    };
    ESP_ERROR_CHECK(esp_now_add_peer(&wifi_espnow_peer_info));

    printf("Wi-Fi: Setup complete\n");
}


void wifi_ieee802111_fsra_tx(uint8_t * data, uint length){
    if (length > MAX_DATA_LENGTH) {
        return;
    }

    uint8_t ieee80211_packet_buffer[MAX_DATA_LENGTH + 32 + 2];

    memcpy(ieee80211_packet_buffer, wifi_ieee80211_raw_header, 32);
    ieee80211_packet_buffer[31] = (length>>8)&0xFF;
    ieee80211_packet_buffer[30] = (length)&0xFF;
    memcpy(ieee80211_packet_buffer + 32, data, length);

    ESP_ERROR_CHECK(esp_wifi_80211_tx(
            WIFI_IF_STA,
            ieee80211_packet_buffer,
            32 + length + 2,
            true)
    );
}


