#ifndef FSRA_CAN_H
#define FSRA_CAN_H

#include <cstring>

#include <driver/spi_master.h>
#include "freertos/task.h"

#include <Arduino.h>
#include "SPI.h"

#include "config.h"
#include "utils.h"

#include "ESP32CAN.h"
#include "CAN_config.h"

#ifdef CAN2_MCP2515
#include "../../CAN_MCP2515/mcp2515.h"
#endif
#ifdef CAN2_MCP2518
#include "ACAN2517FD.h"
#endif

#include "FSRA_COMM/fsra_comm.h"
#include "FSRA_DRS/fsra_drs.h"

#include "pb.h"
#include "pb_common.h"
#include "pb_encode.h"

#include "comm_message.pb.h"





extern QueueHandle_t comm_message_queue;


void can_setup();
void IRAM_ATTR spi_can_isr();

void can_task_handler(void *parameter);
[[noreturn]] void mcp_can_task_handler(void *parameter);

#ifdef CAN2_MCP2515
void mcp2515_setup();
BaseType_t mcp2515_can_queue_send(const can_frame& mcp_rx_frame);
#endif

#ifdef CAN2_MCP2518
void mcp2518_setup();
BaseType_t mcp2518_can_queue_send();
#endif

bool protobuf_can_message_callback(pb_ostream_t *stream, const pb_field_t *field, void * const *arg);

#endif //FSRA_CAN_H
