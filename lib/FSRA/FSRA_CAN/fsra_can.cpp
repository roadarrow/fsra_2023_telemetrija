#include "fsra_can.h"

// CAN 1
CAN_device_t can_device_config;

// CAN 2
#ifdef CAN2_MCP2515
spi_device_handle_t spi_mcp2515;
MCP2515 *mcp2515;
#endif

#ifdef CAN2_MCP2518
SPIClass can_spi;
ACAN2517FD mcp2518(CAN2_CS_PIN, can_spi, GPIO_NUM_34);
#endif

extern volatile SemaphoreHandle_t mcp_can_interrupt_semaphore;

protobuf_can_message protobuf_can_messages[MAX_CAN_MESSAGES];
uint protobuf_can_message_count = 0;

extern TaskHandle_t can_task;
extern TaskHandle_t mcp_can_task;

/*
 * Setup function for CAN devices, initializing internal CAN and MCP2515/8
 */
void can_setup() {
    printf("CAN1: Starting setup...\n");

    can_device_config.speed = CAN1_SPEED;
    can_device_config.tx_pin_id = CAN1_TX_GPIO;
    can_device_config.rx_pin_id = CAN1_RX_GPIO;

    can_device_config.rx_queue = xQueueCreate(CAN_RX_QUEUE_SIZE, sizeof(CAN_frame_t));

    if (ESP32Can.CANInit() != 0) {
        printf("CAN1: ERROR! Failed to initialize\n");
    }
    printf("CAN1: Setup complete\n");

    printf("CAN2: Starting setup...\n");

#ifdef CAN2_MCP2515
    mcp2515_setup();
#endif

#ifdef CAN2_MCP2518
    mcp2518_setup();
#endif

    xTaskCreatePinnedToCore(
            mcp_can_task_handler,   /* Function to implement the task */
            "MCP CAN Task",   /* Name of the task */
            3000,          /* Stack size in words */
            NULL,             /* Task input parameter */
            10,                   /* Priority of the task */
            &mcp_can_task,        /* Task handle. */
            CAN_TASK_RUNNING_CORE);

    xTaskCreatePinnedToCore(
            can_task_handler,   /* Function to implement the task */
            "CAN Task",   /* Name of the task */
            5000,            /* Stack size in words */
            nullptr,             /* Task input parameter */
            8,                   /* Priority of the task */
            &can_task,        /* Task handle. */
            CAN_TASK_RUNNING_CORE);     /* Core where the task should run */

    printf("CAN2: Setup complete\n");

}

/*
 * Setup function initializing SPI CAN transceiver MCP2515
 */
#ifdef CAN2_MCP2515
void mcp2515_setup(){
    spi_bus_config_t bus_config = {
            .mosi_io_num = CAN2_MOSI_PIN,
            .miso_io_num = CAN2_MISO_PIN,
            .sclk_io_num = CAN2_SCLK_PIN,
    }; //!< SPI bus MCP2515 pins
    spi_host_device_t host_id = SPI2_HOST;
    ESP_ERROR_CHECK(spi_bus_initialize(host_id, &bus_config, SPI_DMA_CH_AUTO));

    spi_device_interface_config_t mcp2515_dev_config = {
            .mode = 0, //SPI mode 0
            .clock_speed_hz = 10000000,  // 10 MHz
            .spics_io_num = CAN2_CS_PIN,
            .queue_size = 3,
            .pre_cb = nullptr,
            .post_cb = nullptr,
    }; // MCP2515 SPI device configuration

    ESP_ERROR_CHECK(spi_bus_add_device(host_id, &mcp2515_dev_config, &spi_mcp2515));

    mcp2515 = new MCP2515(&spi_mcp2515);
    if (mcp2515->reset() != MCP2515::ERROR_OK) {
        printf("CAN2: ERROR! Failed to initialize\n");
    }

    pinMode(CAN2_INT_PIN, INPUT_PULLUP);
    attachInterrupt(CAN2_INT_PIN, spi_can_isr, FALLING);
    mcp2515->clearInterrupts();

    mcp2515->setBitrate(CAN_500KBPS, MCP_8MHZ);
#ifdef CAN2_MODE_NORMAL
    mcp2515->setNormalMode();
#endif
#ifdef CAN2_MODE_LISTEN_ONLY
    mcp2515->setListenOnlyMode();
#endif
}
#endif

#ifdef CAN2_MCP2518
/*
 * Setup function initializing SPI CAN transceiver MCP2518
 */
void mcp2518_setup(){
    can_spi.begin(CAN2_SCLK_PIN, CAN2_MISO_PIN, CAN2_MOSI_PIN, CAN2_CS_PIN);

    ACAN2517FDSettings mcp2518_settings (CAN2_CLOCK_CRYSTAL, CAN2_SPEED, DataBitRateFactor::x1);
#ifdef CAN2_MODE_NORMAL
    mcp2518_settings.mRequestedMode = ACAN2517FDSettings::Normal20B;

#endif
#ifdef CAN2_MODE_LISTEN_ONLY
    mcp2518_settings.mRequestedMode = ACAN2517FDSettings::ListenOnly;
#endif

    const uint32_t mcp2518_error_code = mcp2518.begin(mcp2518_settings, spi_can_isr);
    if (mcp2518_error_code != 0) {
        printf("CAN2: ERROR! Failed to initialize, error code %x\n", mcp2518_error_code);
    }

}
#endif

/*
 * ISR run on each CAN interrupt
 */
void IRAM_ATTR spi_can_isr() {
    xSemaphoreGiveFromISR(mcp_can_interrupt_semaphore, NULL);
#ifdef CAN2_MCP2518
    mcp2518.isr();
#endif
}

/*
 * CAN task packing CAN messages into protobuf messages and pushing them to the communications queue
 */
void can_task_handler(void *parameter) {
    for (;;) {

        for (int i = 0; i < MAX_CAN_MESSAGES; i++) {
            protobuf_can_messages[i] = protobuf_can_message{};
        }

        protobuf_can_message_count = 0;
        bool can_received = false;

        for (int i = 0; i < MAX_CAN_MESSAGES; i++) {
            CAN_frame_t can_rx_frame;

            if (xQueueReceive(can_device_config.rx_queue, &can_rx_frame, 10 * portTICK_PERIOD_MS) == pdPASS) {

#if defined CAN_DEBUG || defined DEBUG

                printf("CAN%d: Message! Type: ", can_rx_frame.bus == CAN1 ? 1: 2);
                if (can_rx_frame.FIR.B.FF == CAN_frame_std) {
                    printf("STD");
                } else {
                    printf("EXT");
                }
                if (can_rx_frame.FIR.B.RTR != CAN_RTR) {
                    printf(" ID: 0x%08lX, DLC: %d", can_rx_frame.MsgID, can_rx_frame.FIR.B.DLC);
                    printf(" Data: ");
                    for (int j = 0; j < can_rx_frame.FIR.B.DLC; j++) {
                        printf("%02X ", (char) can_rx_frame.data.u8[j]);
                    }
                } else {
                    printf("RTR ID: 0x%08lX", can_rx_frame.MsgID);
                }
                printf("\n");

#endif
                if (can_rx_frame.FIR.B.RTR != CAN_RTR) {
                    can_received = true;

                    protobuf_can_message protobuf_can_message = protobuf_can_message_init_default;
                    protobuf_can_message.timestamp = utils_get_unix_timestamp();
                    protobuf_can_message.id = can_rx_frame.MsgID;
                    if (can_rx_frame.bus == CAN1) {
                        protobuf_can_message.bus = protobuf_can_message_BUS_CAN1;
                    } else if (can_rx_frame.bus == CAN2) {
                        protobuf_can_message.bus = protobuf_can_message_BUS_CAN2;
                    }

                    memcpy(protobuf_can_message.data.bytes, can_rx_frame.data.u8, can_rx_frame.FIR.B.DLC);
                    protobuf_can_message.data.size = can_rx_frame.FIR.B.DLC;

                    protobuf_can_messages[i] = protobuf_can_message;
                    protobuf_can_message_count++;

                    drs_callback(can_rx_frame);
                }
            } else {
                break; // no CAN messages available in the queue, send the ones received until now
            }
        }

        if (can_received) {
            uint8_t buffer[MAX_DATA_LENGTH];
            pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));

            protobuf_comm_message protobuf_comm_message = protobuf_comm_message_init_default;
            protobuf_comm_message.type = protobuf_comm_message_TYPE_CAN;
            protobuf_comm_message.can_messages.funcs.encode = &protobuf_can_message_callback;
            protobuf_comm_message.has_timestamp = false;

            pb_encode(&stream, protobuf_comm_message_fields, &protobuf_comm_message);

            comm_message_t comm_message;
            memcpy(comm_message.data, buffer, stream.bytes_written);
            comm_message.length = stream.bytes_written;

//            printf("Len: %d\n", comm_message.length);

            xQueueSend(comm_message_queue, &comm_message, 0 * portTICK_PERIOD_MS);
        }

    }
}

#ifdef CAN2_MCP2515
/*
 * Function that packs CAN data to the necessary format for the CAN queue (MCP2515)
 */
BaseType_t mcp2515_can_queue_send(const can_frame& mcp_rx_frame) {
    CAN_frame_t can_rx_frame{};

    if (mcp_rx_frame.can_id & CAN_EFF_FLAG) {
        can_rx_frame.FIR.B.FF = CAN_frame_ext;
        can_rx_frame.MsgID = mcp_rx_frame.can_id & CAN_EFF_MASK;
    } else {
        can_rx_frame.FIR.B.FF = CAN_frame_std;
        can_rx_frame.MsgID = mcp_rx_frame.can_id & CAN_SFF_MASK;
    }

    if (mcp_rx_frame.can_id & CAN_RTR_FLAG) {
        can_rx_frame.FIR.B.RTR = CAN_RTR;
    } else {
        can_rx_frame.FIR.B.RTR = CAN_no_RTR;
    }

    can_rx_frame.FIR.B.DLC = mcp_rx_frame.can_dlc;
    can_rx_frame.bus = CAN2;

    memcpy(can_rx_frame.data.u8, mcp_rx_frame.data, mcp_rx_frame.can_dlc);
    return xQueueSend(can_device_config.rx_queue, &can_rx_frame, 0 * portTICK_PERIOD_MS);
}
#endif

#ifdef CAN2_MCP2518
/*
 * Function that packs CAN data to the necessary format for the CAN queue (MCP2518)
 */
BaseType_t mcp2518_can_queue_send(CANFDMessage mcp2518_frame) {
    CAN_frame_t can_rx_frame{};

    if (mcp2518_frame.ext) {
        can_rx_frame.FIR.B.FF = CAN_frame_ext;
    } else {
        can_rx_frame.FIR.B.FF = CAN_frame_std;
    }
    can_rx_frame.MsgID = mcp2518_frame.id;

    if (mcp2518_frame.type == CANFDMessage::CAN_REMOTE) {
        can_rx_frame.FIR.B.RTR = CAN_RTR;
    } else {
        can_rx_frame.FIR.B.RTR = CAN_no_RTR;
    }

    can_rx_frame.FIR.B.DLC = mcp2518_frame.len;
    can_rx_frame.bus = CAN2;

    memcpy(can_rx_frame.data.u8, mcp2518_frame.data, mcp2518_frame.len);
    return xQueueSend(can_device_config.rx_queue, &can_rx_frame, 0 * portTICK_PERIOD_MS);
}
#endif

#ifdef CAN2_MCP2515
/*
 * Function run on each task call for mcp_can_task (MCP2515)
 */
void mcp2515_can_task_handler(){
    if (xSemaphoreTake(mcp_can_interrupt_semaphore, 500) == pdTRUE) {

        uint8_t irq = mcp2515->getInterrupts();
        mcp2515->clearInterrupts();

        if (irq & MCP2515::CANINTF_RX0IF) {
            struct can_frame mcp_rx_frame{};
            uint8_t result = mcp2515->readMessage(MCP2515::RXB0, &mcp_rx_frame);

            if (result == MCP2515::ERROR_OK) {
                mcp2515_can_queue_send(mcp_rx_frame);
            }
            else {
                printf("CAN2: Error code %d\n", result);
            }
        }

        if (irq & MCP2515::CANINTF_RX1IF) {
            struct can_frame mcp_rx_frame{};
            uint8_t result = mcp2515->readMessage(MCP2515::RXB1, &mcp_rx_frame);

            if (result == MCP2515::ERROR_OK) {
                mcp2515_can_queue_send(mcp_rx_frame);
            }
            else {
                printf("CAN2: Error code %d\n", result);
            }
        }

        uint8_t error = mcp2515->getErrorFlags();

        if (error & MCP2515::EFLG_RX0OVR || error & MCP2515::EFLG_RX1OVR) {
            mcp2515->clearRXnOVRFlags();
        }
    }
    else {
//            mcp2515->reset();
        xSemaphoreGive(mcp_can_interrupt_semaphore);
    }
}
#endif

#ifdef CAN2_MCP2518
/*
 * Function run on each task call for mcp_can_task (MCP2518)
 */
void mcp2518_can_task_handler() {

    if (xSemaphoreTake(mcp_can_interrupt_semaphore, portMAX_DELAY) == pdTRUE) {
        if (mcp2518.available()) {
            CANFDMessage mcp2518_frame;
            mcp2518.receive(mcp2518_frame);

            if (mcp2518_frame.isValid()) {
                mcp2518_can_queue_send(mcp2518_frame);
            }
        }
    }
}
#endif

[[noreturn]] void mcp_can_task_handler(void *parameter) {
    while (true) {
#ifdef CAN2_MCP2515
        mcp2515_can_task_handler();
#endif
#ifdef CAN2_MCP2518
        mcp2518_can_task_handler();
#endif
    }
}

bool protobuf_can_message_callback(pb_ostream_t *stream, const pb_field_t *field, void *const *arg) {
    for (int i = 0; i < protobuf_can_message_count; i++) {

        if (!pb_encode_tag_for_field(stream, field)) {
            return false;
        }
        if (!pb_encode_submessage(stream, protobuf_can_message_fields, &protobuf_can_messages[i])) {
            return false;
        }
    }
    return true;
}