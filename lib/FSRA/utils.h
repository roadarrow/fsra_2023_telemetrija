#ifndef UTILS_H
#define UTILS_H

#include <Arduino.h>

#include <sys/time.h>
#include <esp_now.h>

#include "config.h"

typedef struct {
    uint8_t data[MAX_DATA_LENGTH];
    uint16_t length;
} comm_message_t;

uint64_t utils_get_unix_timestamp();

#endif //UTILS_H
