#include "utils.h"

uint64_t utils_get_unix_timestamp() {
    struct timeval tv{};
    gettimeofday(&tv, nullptr);
    return (tv.tv_sec * 1000000LL + (tv.tv_usec / 1LL));
}
