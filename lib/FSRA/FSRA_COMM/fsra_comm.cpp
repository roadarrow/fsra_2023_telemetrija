#include "fsra_comm.h"

extern bool eth_connected;
extern AsyncUDP eth_udp;

extern volatile SemaphoreHandle_t comm_heartbeat_timer_semaphore;
extern volatile SemaphoreHandle_t sd_flush_timer_semaphore;
extern bool ftp_connected;

extern TaskHandle_t comm_task;
uint8_t wifi_broadcast_address[] = WIFI_BROADCAST_ADDRESS;


void IRAM_ATTR comm_heartbeat_timer_callback() {
    xSemaphoreGiveFromISR(comm_heartbeat_timer_semaphore, NULL);
    //xSemaphoreGiveFromISR(sd_flush_timer_semaphore, NULL);
}

void comm_setup() {
    xTaskCreatePinnedToCore(
            comm_task_handler,   /* Function to implement the task */
            "Comm Task",   /* Name of the task */
            5000,          /* Stack size in words */
            NULL,             /* Task input parameter */
            COMM_TASK_PRIORITY,                   /* Priority of the task */
            &comm_task,        /* Task handle. */
            COMM_TASK_RUNNING_CORE);     /* Core where the task should run */
}

void comm_task_handler(void *parameter) {
    for (;;) {
        comm_message_t comm_message;

        // communication
        if (xQueueReceive(comm_message_queue,
                          &comm_message,
                          500 * portTICK_PERIOD_MS) == pdPASS) {

            if (!eth_connected) {
#ifdef USE_ESP_NOW
                esp_now_send(wifi_broadcast_address,
                             comm_message.length != 0 ? comm_message.data : (uint8_t *) "",
                             comm_message.length != 0 ? comm_message.length : 1);
                // checks if the message isn't empty to prevent ESP-NOW bugs
#endif
#ifdef USE_FSRA_IEEE80211
                wifi_ieee802111_fsra_tx(
                        comm_message.length != 0 ? comm_message.data : (uint8_t *) "",
                        comm_message.length != 0 ? comm_message.length : 1
                        );
#endif
            }
            else {
                eth_udp.writeTo(comm_message.data,
                                comm_message.length,
                                eth_server_ip_address,
                                eth_udp_port,
                                TCPIP_ADAPTER_IF_ETH);
            }
        }

            // heartbeat
        else if (xSemaphoreTake(comm_heartbeat_timer_semaphore, 0) == pdTRUE) {
            protobuf_comm_message protobuf_comm_message = protobuf_comm_message_init_default;
            protobuf_comm_message.type = protobuf_comm_message_TYPE_HEARTBEAT;
            protobuf_comm_message.has_timestamp = true;
            protobuf_comm_message.has_gnss_message = false;
            protobuf_comm_message.timestamp = utils_get_unix_timestamp();

            uint8_t protobuf_buffer[MAX_DATA_LENGTH];
            pb_ostream_t protobuf_stream = pb_ostream_from_buffer(protobuf_buffer, sizeof(protobuf_buffer));

            pb_encode(&protobuf_stream, protobuf_comm_message_fields, &protobuf_comm_message);

            memcpy(comm_message.data, protobuf_buffer, protobuf_stream.bytes_written);
            comm_message.length = protobuf_stream.bytes_written;

            xQueueSend(comm_message_queue, &comm_message, 0 * portTICK_PERIOD_MS);
        }
    }
}
