#ifndef FSRA_COMM_H
#define FSRA_COMM_H

#include <Arduino.h>



#include "pb.h"
#include "pb_common.h"
#include "pb_encode.h"
#include "comm_message.pb.h"

#include "FSRA_ETH/fsra_eth.h"
#include "FSRA_WIFI/fsra_wifi.h"
#include "FSRA_SD/fsra_sd.h"

extern QueueHandle_t comm_message_queue;


void IRAM_ATTR comm_heartbeat_timer_callback();

void comm_setup();
void comm_task_handler(void *parameter);

#endif //FSRA_COMM_H
