#ifndef FSRA_OTA_H
#define FSRA_OTA_H

#include <Arduino.h>
#include "ArduinoOTA.h"
#include <WiFi.h>
#include <ESPmDNS.h>
#include <WiFiUdp.h>

#include "config.h"

void ota_setup();
void ota_handle();

#endif //FSRA_OTA_H
