#include "fsra_ota.h"

extern bool eth_connected;


void ota_setup() {
    printf("OTA: Setup Started...\n");

    ArduinoOTA
            .onStart([]() {
                String type;
                if (ArduinoOTA.getCommand() == U_FLASH)
                    type = "sketch";
                else // U_SPIFFS
                    type = "filesystem";

                // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
                Serial.println("OTA: Start updating " + type);
            })
            .onEnd([]() {
                Serial.println("\nOTA: End");
            })
            .onProgress([](unsigned int progress, unsigned int total) {
                Serial.printf("OTA: Progress: %u%%\r", (progress / (total / 100)));
            })
            .onError([](ota_error_t error) {
                Serial.printf("OTA: Error[%u]: ", error);
                if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
                else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
                else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
                else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
                else if (error == OTA_END_ERROR) Serial.println("End Failed");
            });

    ArduinoOTA.setHostname(OTA_MDNS_HOSTNAME);
    ArduinoOTA.setMdnsEnabled(true);
    ArduinoOTA.begin();

    printf("OTA: Setup Complete!\n");
}

void ota_handle() {
    if (eth_connected){
        ArduinoOTA.handle();
    }
}