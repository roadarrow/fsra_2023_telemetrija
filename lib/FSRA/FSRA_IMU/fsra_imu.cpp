//#include "fsra_imu.h"
//
//struct bno055_t imu_interface;
//extern volatile SemaphoreHandle_t i2c_mutex;
//
//void imu_setup(){
//    xSemaphoreTake(i2c_mutex, 0);
//    BNO_Init(&imu_interface);
//    bno055_set_operation_mode(OPERATION_MODE_NDOF);
//    xSemaphoreGive(i2c_mutex);
//}
//
//void imu_task_handler(void* parameters){
//    for(;;) {
//        if( xSemaphoreTake(i2c_mutex, 0) == pdTRUE) {
//            bno055_euler myEulerData{};
//            bno055_read_euler_hrp(&myEulerData);            //Update Euler data into the structure
//
//            Serial.print("Heading(Yaw): ");                //To read out the Heading (Yaw)
//            Serial.println(float(myEulerData.h) / 16.00);        //Convert to degrees
//
//            Serial.print("Roll: ");                    //To read out the Roll
//            Serial.println(float(myEulerData.r) / 16.00);        //Convert to degrees
//
//            Serial.print("Pitch: ");                //To read out the Pitch
//            Serial.println(float(myEulerData.p) / 16.00);        //Convert to degrees
//
//            Serial.println();
//            xSemaphoreGive(i2c_mutex);
//        }
//        delay(1000);
//    }
//}