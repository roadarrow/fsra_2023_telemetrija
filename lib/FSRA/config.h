#ifndef CONFIG_H
#define CONFIG_H

#include <Arduino.h>

#include <CAN_config.h>

#include "WiFi.h"
#include "esp_wifi.h"

//TODO: Implement macros

/* ---------------- DEBUG ---------------- */
//#define DEBUG
//#define CAN_DEBUG
//#define GNSS_DEBUG
//#define DRS_DEBUG

/* ---------------- OTA ---------------- */
#define OTA_ENABLED
#define OTA_MDNS_HOSTNAME           "fsra-telemetry-ota"

/* ---------------- SERIAL ---------------- */

#define SERIAL_BAUD_RATE            115200

/* ---------------- CAN ---------------- */
#define CAN_ENABLED

#define CAN_TASK_RUNNING_CORE       1
#define CAN_TASK_PRIORITY           5
#define CAN_RX_QUEUE_SIZE           50

#define CAN1_SPEED                  CAN_SPEED_500KBPS
#define CAN1_TX_GPIO                GPIO_NUM_5
#define CAN1_RX_GPIO                GPIO_NUM_35

// SELECT CAN CHIP TYPE
//#define CAN2_MCP2515
#define CAN2_MCP2518

#define CAN2_MOSI_PIN               GPIO_NUM_2
#define CAN2_MISO_PIN               GPIO_NUM_15
#define CAN2_SCLK_PIN               GPIO_NUM_14
#define CAN2_CS_PIN                 GPIO_NUM_17
#define CAN2_INT_PIN                GPIO_NUM_34
#define CAN2_MODE_NORMAL
//#define CAN2_MODE_LISTEN_ONLY

#ifdef CAN2_MCP2515
#define CAN2_SPEED                  CAN_500KBPS
#define CAN2_CLOCK_CRYSTAL          MCP_8MHZ
#endif
#ifdef CAN2_MCP2518
#define CAN2_SPEED                  (500UL * 1000UL)
#define CAN2_CLOCK_CRYSTAL          ACAN2517FDSettings::OSC_40MHz
#endif

#ifdef CAN2_MCP2515
#ifdef CAN2_MCP2518
#error  "CAN: Both CAN transcievers defined!"
#endif
#endif


/* ---------------- Wi-Fi ---------------- */

// Wi-Fi TX mode
#define USE_ESP_NOW
//#define USE_FSRA_IEEE80211


#define WIFI_PHY_DATA_RATE          WIFI_PHY_RATE_11M_L
#define WIFI_PHY_TX_POWER           WIFI_POWER_19_5dBm
#define WIFI_CHANNEL                0
#define WIFI_BROADCAST_ADDRESS      {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF}

/* ---------------- ETHERNET ----------------*/

const IPAddress eth_local_ip_address = IPAddress(192, 168, 1, 2);
const IPAddress eth_gateway_ip_address = IPAddress(192, 168, 1, 1);
const IPAddress eth_subnet_mask = IPAddress(255, 255, 255, 0);
const IPAddress eth_dns_ip_address = IPAddress(192, 168, 1, 1);
const uint16_t eth_udp_port = 8888;
const IPAddress eth_server_ip_address = IPAddress(192, 168, 1, 255);

#define ETH_ENABLED

// Ethernet communication mode
//#define ETH_UNICAST
#define ETH_MULTICAST

#define ETH_LOCAL_IP_ADDRESS        IPAddress(192, 168, 1, 2)
#define ETH_GATEWAY_IP_ADDRESS      IPAddress(192, 168, 1, 1)
#define ETH_SUBNET_MASK             IPAddress(255, 255, 255, 0)
#define ETH_DNS_IP_ADDRESS          IPAddress(192, 168, 1, 1)
#define ETH_UDP_PORT                8888
#define ETH_SERVER_IP_ADDRESS       IPAddress(192, 168, 1, 25)
#define ETH_BROADCAST_IP_ADDRESS    IPAddress(192, 168, 1, 255)
#ifdef ETH_UNICAST
#define ETH_DESTINATION_IP_ADDRESS ETH_SERVER_IP_ADDRESS
#endif
#ifdef ETH_MULTICAST
#define ETH_DESTINATION_IP_ADDRESS ETH_BROADCAST_IP_ADDRESS
#endif

/* ---------------- COMMUNICATION ---------------- */
//#define DATA_RATE_TEST
#define COMM_TASK_RUNNING_CORE      0
#define COMM_TASK_PRIORITY          5
#ifdef USE_ESP_NOW
#define COMM_MESSAGE_QUEUE_SIZE     30
#endif
#ifdef USE_FSRA_IEEE80211
#define COMM_MESSAGE_QUEUE_SIZE     10
#endif
#define COMM_HEARTBEAT_INTERVAL     500000

#ifdef USE_ESP_NOW
#define MAX_DATA_LENGTH ESP_NOW_MAX_DATA_LEN
#define MAX_CAN_MESSAGES 8
#endif
#ifdef USE_FSRA_IEEE80211
#define MAX_DATA_LENGTH 700
#define MAX_CAN_MESSAGES 16
#endif

/* ---------------- GNSS ---------------- */
#define GNSS_TASK_RUNNING_CORE      1
#define GNSS_TASK_PRIORITY          3
#define GNSS_BAUD_RATE              921600
#define GNSS_RX_GPIO                GPIO_NUM_36
#define GNSS_TX_GPIO                GPIO_NUM_4

#define GNSS_START_TIME             1672531200UL

/* ---------------- RTC ---------------- */
#define RTC_UPDATE_INTERVAL         (60*1000000) //! 60 seconds

/* ---------------- DRS ---------------- */
//#define DRS_ENABLED
//#define DRS_TEST

#define DRS_TASK_RUNNING_CORE       1
#define DRS_SERVO_GPIO              GPIO_NUM_12

#define DRS_CONTROL_TIMEOUT         500

#define DRS_SERVO_OPENED_OFFSET     15
#define DRS_SERVO_CLOSED_OFFSET     (-15)

#define DRS_SERVO_MAX_ANGLE         360
#define DRS_SERVO_MIN_WIDTH_US      500
#define DRS_SERVO_MAX_WIDTH_US      2500
#define DRS_SERVO_FREQUENCY         200


#endif //CONFIG_H
