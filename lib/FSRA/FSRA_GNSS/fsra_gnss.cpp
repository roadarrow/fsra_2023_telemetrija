#include <esp_now.h>
#include "fsra_gnss.h"
#include "utils.h"
#include "FSRA_COMM/fsra_comm.h"


TinyGPSPlus gnss_interface;
RTC_DS3231 rtc_interface;

bool rtc_lost_power = false;
extern volatile SemaphoreHandle_t rtc_update_timer_semaphore;

extern TaskHandle_t gnss_task;

void IRAM_ATTR rtc_update_timer_callback() {
    xSemaphoreGiveFromISR(rtc_update_timer_semaphore, NULL);
}

void gnss_setup() {
    // set the time to UNIX epoch
    timeval rtc_time = {0, 0};

    if (!rtc_interface.begin()) {
        printf("RTC: Not connected!\n");
    }
    if (!rtc_interface.lostPower()) {
        uint32_t rtc_unix_time = rtc_interface.now().unixtime();
        printf("RTC: Current UNIX time is: %u s\n", rtc_unix_time);
        rtc_time.tv_sec = static_cast<time_t>(rtc_unix_time);
        settimeofday((const timeval *) &rtc_time, nullptr);
    } else {
        rtc_lost_power = true;
        rtc_interface.adjust(DateTime(rtc_time.tv_sec));
        printf("RTC: Time not available, waiting for GNSS time\n");
    }

    Serial2.begin(GNSS_BAUD_RATE, SERIAL_8N1, GNSS_RX_GPIO, GNSS_TX_GPIO);

    xTaskCreatePinnedToCore(
            gnss_task_handler,   /* Function to implement the task */
            "GNSS Task",   /* Name of the task */
            2500,          /* Stack size in words */
            nullptr,             /* Task input parameter */
            GNSS_TASK_PRIORITY,                   /* Priority of the task */
            &gnss_task,        /* Task handle. */
            GNSS_TASK_RUNNING_CORE);     /* Core where the task should run */
}

[[noreturn]] void gnss_task_handler(void *parameter) {
    delay(1000);
    while(true) {

        while (Serial2.available() > 0) {
            gnss_interface.encode(Serial2.read());
        }

        if (gnss_interface.date.isUpdated() && gnss_interface.time.isUpdated()
            && gnss_interface.time.isValid() && gnss_interface.date.isValid()) {

            DateTime gnss_time(
                    gnss_interface.date.year(),
                    gnss_interface.date.month(),
                    gnss_interface.date.day(),
                    gnss_interface.time.hour(),
                    gnss_interface.time.minute(),
                    gnss_interface.time.second()
            );  //Time elements structure

            timeval gnss_unix_time = {0, 0};
            suseconds_t gnss_microseconds = gnss_interface.time.centisecond() * 10000;

            if (gnss_time.isValid()) {
                gnss_unix_time = {static_cast<time_t>(gnss_time.unixtime()), gnss_microseconds};
            }

            // adjusting system time from GNSS
            // Fri Dec 31 1999 00:00:00 GMT+0000
            if (gnss_unix_time.tv_sec > GNSS_START_TIME) {
                // if RTC is not set, set the time to GNSS time

                if (rtc_lost_power || xSemaphoreTake(rtc_update_timer_semaphore, 0) == pdTRUE) {
                    rtc_interface.adjust(gnss_time);
                    rtc_lost_power = false;
                }

                // set the internal RTC to GNSS time
                settimeofday((const timeval *) &gnss_unix_time, nullptr);
            }
#if defined GNSS_DEBUG || defined DEBUG
                Serial.print(F("TIME       Fix Age="));
                Serial.print(gnss_interface.time.age());
                Serial.print(F("ms Raw="));
                Serial.print(gnss_interface.time.value());
                Serial.print(F(" Hour="));
                Serial.print(gnss_interface.time.hour());
                Serial.print(F(" Minute="));
                Serial.print(gnss_interface.time.minute());
                Serial.print(F(" Second="));
                Serial.print(gnss_interface.time.second());
                Serial.print(F(" Hundredths="));
                Serial.println(gnss_interface.time.centisecond());
                printf("Unix Timestamp = %llu\n", utils_get_unix_timestamp());
#endif
        }
        // TODO: Add GNSS diagnostics(check if connected) and add satellite info

        // changed && to || TODO: Test this
        bool gnss_updated = gnss_interface.location.isUpdated() && gnss_interface.speed.isUpdated() &&
                              gnss_interface.speed.isUpdated() && gnss_interface.altitude.isUpdated() &&
                              gnss_interface.hdop.isUpdated();

        bool gnss_valid = gnss_interface.location.isValid() && gnss_interface.speed.isValid() &&
                            gnss_interface.speed.isValid() && gnss_interface.altitude.isValid() &&
                            gnss_interface.hdop.isValid();


        if (gnss_updated && gnss_valid) {
            protobuf_comm_message protobuf_comm_message = protobuf_comm_message_init_default;
            protobuf_comm_message.type = protobuf_comm_message_TYPE_GNSS;
            protobuf_comm_message.has_gnss_message = true;
            protobuf_comm_message.has_timestamp = true;

            protobuf_comm_message.timestamp = utils_get_unix_timestamp();
            protobuf_comm_message.gnss_message.latitude = gnss_interface.location.lat();
            protobuf_comm_message.gnss_message.longitude = gnss_interface.location.lng();
            protobuf_comm_message.gnss_message.speed = gnss_interface.speed.kmph();
            protobuf_comm_message.gnss_message.heading = gnss_interface.course.deg();
            protobuf_comm_message.gnss_message.altitude = gnss_interface.altitude.meters();
            protobuf_comm_message.gnss_message.horizontal_precision = gnss_interface.hdop.hdop();

            uint8_t protobuf_buffer[MAX_DATA_LENGTH];
            pb_ostream_t protobuf_stream = pb_ostream_from_buffer(protobuf_buffer, sizeof(protobuf_buffer));

            pb_encode(&protobuf_stream, protobuf_comm_message_fields, &protobuf_comm_message);

            comm_message_t comm_message;
            memcpy(comm_message.data, protobuf_buffer, protobuf_stream.bytes_written);
            comm_message.length = protobuf_stream.bytes_written;

            xQueueSend(comm_message_queue, &comm_message, 0 * portTICK_PERIOD_MS);
        }

        if (gnss_interface.charsProcessed() < 10)
            printf("GNSS: No data recieved.  Check wiring.\n");

        delay(10);
    }
}

