#ifndef FSRA_GNSS_H
#define FSRA_GNSS_H

#include <Arduino.h>
#include <SPI.h>

#include "TinyGPSPlus.h"

//#include "BNO055_support.h"

#include <sys/time.h>
#include "TimeLib.h"
#include "RTClib.h"

#include <Wire.h>

#include "pb.h"
#include "pb_common.h"
#include "pb_encode.h"

#include "comm_message.pb.h"

#include "config.h"


void gnss_setup();

[[noreturn]] void gnss_task_handler(void *parameter);
void rtc_update_timer_callback();

#endif //FSRA_GNSS_H
