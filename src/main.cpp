// libraries
#include <Arduino.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "FSRA_CAN/fsra_can.h"
#include "FSRA_WIFI/fsra_wifi.h"
#include "FSRA_ETH/fsra_eth.h"
#include "FSRA_COMM/fsra_comm.h"
#include "FSRA_GNSS/fsra_gnss.h"
#include "FSRA_SD/fsra_sd.h"
#include "FSRA_OTA/fsra_ota.h"
#include "config.h"
#include "utils.h"

#include <HardwareSerial.h>
#include <TimeLib.h>


/* -------- TASK HANDLES -------- */
TaskHandle_t configuration_task;
TaskHandle_t can_task;
TaskHandle_t comm_task;
TaskHandle_t gnss_task;
TaskHandle_t mcp_can_task;
TaskHandle_t drs_task;


// queues
//QueueHandle_t can_message_queue = xQueueCreate(can_rx_queue_size, sizeof(can_message_t));
DRAM_ATTR QueueHandle_t comm_message_queue = xQueueCreate(COMM_MESSAGE_QUEUE_SIZE, sizeof(comm_message_t));
DRAM_ATTR QueueHandle_t drs_command_queue = xQueueCreate(1, sizeof(DRS_State));

// timers
hw_timer_t* comm_heartbeat_timer = nullptr;
hw_timer_t* rtc_update_timer = nullptr;

// semaphores
SemaphoreHandle_t comm_heartbeat_timer_semaphore;
SemaphoreHandle_t sd_flush_timer_semaphore;
SemaphoreHandle_t rtc_update_timer_semaphore;
SemaphoreHandle_t mcp_can_interrupt_semaphore;


void configuration_task_handler(void *parameter) {
    comm_heartbeat_timer_semaphore = xSemaphoreCreateBinary();
    sd_flush_timer_semaphore = xSemaphoreCreateBinary();
    rtc_update_timer_semaphore = xSemaphoreCreateBinary();
    mcp_can_interrupt_semaphore = xSemaphoreCreateBinary();

    Serial.begin(SERIAL_BAUD_RATE);
    printf("FSRA Telemetry unit\n");
    printf("%s %s\n", __DATE__, __TIME__);

    printf("GNSS: Starting setup...\n");
    gnss_setup();
    printf("GNSS: Setup complete\n");

    rtc_update_timer = timerBegin(1, 80, true);
    timerAttachInterrupt(rtc_update_timer, &rtc_update_timer_callback, true);
    timerAlarmWrite(rtc_update_timer, RTC_UPDATE_INTERVAL, true);
    timerAlarmEnable(rtc_update_timer);

#ifdef SD_ENABLED
    sd_setup();
#endif

#ifdef CAN_ENABLED
    can_setup();
#endif

    wifi_setup();

    comm_setup();
    comm_heartbeat_timer = timerBegin(0, 80, true);
    timerAttachInterrupt(comm_heartbeat_timer, &comm_heartbeat_timer_callback, true);
    timerAlarmWrite(comm_heartbeat_timer, COMM_HEARTBEAT_INTERVAL, true);
    timerAlarmEnable(comm_heartbeat_timer);

#ifdef DRS_ENABLED
    drs_setup();
#endif

    delay(500); // Ethernet delay

    eth_setup();

#ifdef OTA_ENABLED
    ota_setup();
#endif


    vTaskDelete(nullptr);
}

void setup() {
    xTaskCreatePinnedToCore(
            configuration_task_handler,   /* Function to implement the task */
            "Configuration Task",   /* Name of the task */
            3000,          /* Stack size in words */
            nullptr,             /* Task input parameter */
            15,                   /* Priority of the task */
            &configuration_task,        /* Task handle. */
            1);
}

void loop() {

#ifdef OTA_ENABLED
    ota_handle();
#else
    vTaskDelete(nullptr);
#endif

}




